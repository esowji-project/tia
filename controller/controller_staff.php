<?php

	if(isset($_GET['page'])){
		$page = $_GET['page'];
 
		switch ($page) {
			case 'Home':
				include "page/home/home.php";
				break;
			case 'Document':
				include "page/document/document.php";
				break;
			case 'Document/Add':
				include "page/document/document_add.php";
				break;
			case 'Document/Category':
				include "page/document-category/category.php";
				break;
			case 'Document/Type':
				include "page/document-type/type.php";
				break;	
			default:
				echo "<script>alert('Halaman Tidak Ditemukan !');window.location='?page=Home'</script>";
				break;
		}
	}else{
		include "page/home/home.php";
	}

?>