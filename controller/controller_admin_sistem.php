<?php
	if(isset($_GET['page'])){
		$page = $_GET['page'];
 
		switch ($page) {

			// Controller Home
			case 'Home':
				include "page/home/home.php";
				break;

			// ..................................................................
			// Controller Master 

			// User
			case 'User':
				include "page/user/user.php";
				break;

			// Departement
			case 'Departemen':
				include "page/departemen/departemen.php";
				break;	

			// Document
			case 'Document':
				include "page/document/document.php";
				break;

			// Category
			case 'Document/Category':
				include "page/document-category/category.php";
				break;

			// Type
			case 'Document/Type':
				include "page/document-type/type.php";
				break;

			// ..................................................................
			// Controller Add

			// User
			case 'User/Add':
				include "page/user/user_add.php";
				break;

			// Document
			case 'Document/Add':
				include "page/document/document_add.php";
				break;

			// Category
			case 'Document/Category/Add':
				include "page/document-category/category_add.php";
				break;

			// Type
			case 'Document/Type/Add':
				include "page/document-type/type_add.php";
				break;

			// Departemen
			case 'Departemen/Add':
				include "page/departemen/departemen_add.php";
				break;

			// ..................................................................
			// Controller Edit

			// User
			case 'User/Edit':
				include "page/user/user_edit.php";
				break;

			// Document
			case 'Document/Edit':
				include "page/document/document_edit.php";
				break;

			// Category
			case 'Document/Category/Edit':
				include "page/document-category/category_edit.php";
				break;

			// Type
			case 'Document/Type/Edit':
				include "page/document-type/type_edit.php";
				break;

			// Departemen
			case 'Departemen/Edit':
				include "page/departemen/departemen_edit.php";
				break;

			// ..................................................................
			// Controller Detail

			// User
			case 'User/Detail':
				include "page/user/user_detail.php";
				break;

			// ..................................................................
			// Page Not Found
			default:
				echo "<script>alert('Page Not Found !');window.location='?page=Home'</script>";
				break;
		}

	} else {

		include "page/home/home.php";

	}

	

?>