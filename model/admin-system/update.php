<?php 
include '../../config.php';
if(isset($_GET['admin-system'])){
	$admin = $_GET['admin-system'];

	if($admin=="UpdateDataUser"){

		$id_user 			= $_post['id_user'];
		$username 			= $_POST['username'];
		$password 			= $_POST['password'];
		$level 				= $_POST['level'];

		$name 				= $_POST['name'];
		$address 			= $_POST['address'];
		$email 				= $_POST['email'];
		$phone 				= $_POST['phone'];
		$departemen_id 		= $_POST['departemen_id'];
		$user_created_at 	= $_POST['user_created_at'];
		$user_update_at 	= $_POST['user_update_at'];
		$user_created_by 	= $_POST['user_created_by'];
		$user_update_by 	= $_POST['user_update_by'];
		$status_employe 	= $_POST['status_employe'];

		$cek = mysqli_query($conn, "SELECT name FROM tb_employe WHERE name = '$name' ");

		if (mysqli_num_rows($cek) >= 2) {
			echo "<script>alert('Data Already Exists.');window.location='../../view/admin/index.php?page=Document/Type'</script>";
		} else {

			mysqli_query($conn, "UPDATE tb_employe SET id_user = '$iduser', name = '$name', address = '$address', email = '$email', phone = '$phone', departemen_id = '$departemen_id', user_created_at = '$user_created_at', user_update_at = '$user_update_at', user_created_by = '$user_created_by', user_update_by = '$user_update_by', status_user = '$status_employe' WHERE id_user = '$id_user'");

			mysqli_query($conn, "UPDATE tb_user SET username = '$username', password = '$password', level = '$level', status_user = '$status_employe' WHERE id_user = '$id_user'");
				
			echo "<script>alert('Update Data Successfully.');window.location='../../view/admin/index.php?page=User'</script>";
		}

	} elseif ($admin=="UpdateDataDepartemen") {

		$departemen_id 			= $_POST['departemen_id'];
		$departemen_code 		= $_POST['departemen_code'];
		$departemen_name 		= $_POST['departemen_name'];
		$departemen_created_at 	= $_POST['departemen_created_at'];
		$departemen_created_by 	= $_POST['departemen_created_by'];
		$departemen_update_at 	= $_POST['departemen_update_at'];
		$departemen_update_by 	= $_POST['departemen_update_by'];
		$departemen_status 		= $_POST['departemen_status'];

		$cek = mysqli_query($conn, "SELECT departemen_name FROM tb_departemen WHERE departemen_name = '$departemen_name' ");

		if (mysqli_num_rows($cek) >= 2) {
			echo "<script>alert('Data Already Exists.');window.location='../../view/admin/index.php?page=Document/Type'</script>";
		} else {

			mysqli_query($conn, "UPDATE tb_departemen SET departemen_code = '$departemen_code', departemen_name = '$departemen_name', departemen_created_at = '$departemen_created_at', departemen_created_by = '$departemen_created_by', departemen_update_at = '$departemen_update_at', departemen_update_by = '$departemen_update_by', departemen_status = '$departemen_status' WHERE departemen_id = '$departemen_id' ");
			
			echo "<script>alert('Update Data Successfully.');window.location='../../view/admin/index.php?page=Departemen'</script>";
		}

	} elseif ($admin=="UpdateDataDocCategory") {

		$doc_category_id 			= $_POST['doc_category_id'];
		$doc_category_code 			= $_POST['doc_category_code'];
		$doc_category_name 			= $_POST['doc_category_name'];
		$doc_category_created_at 	= $_POST['doc_category_created_at'];
		$doc_category_created_by 	= $_POST['doc_category_created_by'];
		$doc_category_update_at 	= $_POST['doc_category_update_at'];
		$doc_category_update_by 	= $_POST['doc_category_update_by'];
		$doc_category_status 		= $_POST['doc_category_status'];

		$cek = mysqli_query($conn, "SELECT doc_category_name FROM tb_document_category WHERE doc_category_name = '$doc_category_name' ");

		if (mysqli_num_rows($cek) >= 2) {
			echo "<script>alert('Data Already Exists.');window.location='../../view/admin/index.php?page=Document/Type'</script>";
		} else {

			mysqli_query($conn, "UPDATE tb_document_category SET doc_category_code = '$doc_category_code', doc_category_name = '$doc_category_name', doc_category_created_at = '$doc_category_created_at', doc_category_created_by = '$doc_category_created_by', doc_category_update_at = '$doc_category_update_at', doc_category_update_by = '$doc_category_update_by' WHERE doc_category_id = '$doc_category_id' ");
			
			echo "<script>alert('Update Data Successfully.');window.location='../../view/admin/index.php?page=Document/Category'</script>";
		}

	} elseif ($admin=="UpdateDataDocType") {

		$doc_type_id 			= $_POST['doc_type_id'];
		$doc_type_name 			= $_POST['doc_type_name'];
		$doc_type_created_at 	= $_POST['doc_type_created_at'];
		$doc_type_created_by 	= $_POST['doc_type_created_by'];
		$doc_type_update_at 	= $_POST['doc_type_update_at'];
		$doc_type_update_by 	= $_POST['doc_type_update_by'];
		$doc_type_status 		= $_POST['doc_type_status'];

		$cek = mysqli_query($conn, "SELECT doc_type_name FROM tb_document_type WHERE doc_type_name = '$doc_type_name' ");

		if (mysqli_num_rows($cek) >= 2) {
			echo "<script>alert('Data Already Exists.');window.location='../../view/admin/index.php?page=Document/Type'</script>";
		} else {

			mysqli_query($conn, "UPDATE tb_document_type SET doc_type_name = '$doc_type_name', doc_type_created_at = '$doc_type_created_at', doc_type_created_by = '$doc_type_created_by', doc_type_update_at = '$doc_type_update_at', doc_type_update_by = '$doc_type_update_by' WHERE doc_type_id = '$doc_type_id' ");
			
			echo "<script>alert('Update Data Successfully.');</script>";
		}
	} 
}