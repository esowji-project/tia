<?php

	/**
	 * summary
	 */

	class DataDisplayAdminSystem
	{

		// Show Data

		function SelectUser() {
	    	require("../../config.php");
	    	$sql 	= mysqli_query($conn, "SELECT * FROM tb_user INNER JOIN (tb_employe INNER JOIN tb_departemen ON tb_employe.departemen_id = tb_departemen.departemen_id) ON tb_user.id_user = tb_employe.id_user");
	    	while ($row = mysqli_fetch_array($sql))
	    	    $data[] = $row;
	    	return $data;
	    }


	    function SelectDepartemen() {
	    	require("../../config.php");
	    	$sql 	= mysqli_query($conn, "SELECT * FROM tb_departemen INNER JOIN tb_employe ON tb_departemen.departemen_created_by = tb_employe.employe_id");
	    	while ($row = mysqli_fetch_array($sql))
	    	    $data[] = $row;
	    	return $data;
	    }

	    function SelectCategory() {
	    	require("../../config.php");
	    	$sql 	= mysqli_query($conn, "SELECT * FROM tb_document_category INNER JOIN tb_document_type ON tb_document_category.doc_type_id = tb_document_type.doc_type_id ");
	    	while ($row = mysqli_fetch_array($sql))
	    	    $data[] = $row;
	    	return $data;
	    }
	    
	    function SelectType() {
	    	require("../../config.php");
	    	$sql 	= mysqli_query($conn, "SELECT * FROM tb_document_type INNER JOIN tb_employe ON tb_document_type.doc_type_created_by = tb_employe.id_user ");
	    	while ($row = mysqli_fetch_array($sql))
	    	    $data[] = $row;
	    	return $data;
	    }

	    // Show Detail

	    // function DetailUser() {
	    // 	require("../../config.php");
	    // 	$sql 	= mysqli_query($conn, "SELECT * FROM tb_user INNER JOIN tb_departemen ON tb_user.departemen_id = tb_departemen.departemen_id WHERE tb_user.id_user = '$id' ");
	    // 	while ($row = mysqli_fetch_array($sql))
	    // 	    $data = $row['id_user'];
	    // 	return $data;
	    // }


	    // Show Code Otomatis

	    function CodeUser() {
	    	require("../../config.php");

			$query 		= mysqli_query($conn, "SELECT max(id_user) AS code FROM tb_user ");
			$data 		= mysqli_fetch_array($query);
			$row 		= $data['code'];
			$new 		= $row + 1;

	    	return $new;
	    }

	    function CodeCategory() {
	    	require("../../config.php");

			$query 		= mysqli_query($conn, "SELECT max(doc_category_code) AS code FROM tb_document_category ");
			$data 		= mysqli_fetch_array($query);
			$row 		= $data['code'];
			$new 		= $row + 1;

	    	return $new;
	    }


	    // Show Data Edit

	    function SelectUserEdit() {
	    	require("../../config.php");
	    	$id 	= $_SESSION['employe_id'];
	    	$sql 	= mysqli_query($conn, "SELECT * FROM tb_user INNER JOIN (tb_employe INNER JOIN tb_departemen ON tb_employe.departemen_id = tb_departemen.departemen_id) ON tb_user.id_user = tb_employe.id_user WHERE tb_employe.employe_id = '$id' ");
	    	while ($row = mysqli_fetch_array($sql))
	    	    $data[] = $row;
	    	return $data;
	    }

	    function SelectDepartemenEdit() {
	    	require("../../config.php");
	    	$id 	= $_SESSION['departemen_id'];
	    	$sql 	= mysqli_query($conn, "SELECT * FROM tb_departemen INNER JOIN tb_employe ON tb_departemen.departemen_created_by = tb_employe.employe_id WHERE departemen_id = '$id' ");
	    	while ($row = mysqli_fetch_array($sql))
	    	    $data[] = $row;
	    	return $data;
	    }

	    function SelectCategoryEdit() {
	    	require("../../config.php");
	    	$id 	= $_SESSION['doc_category_id'];
	    	$sql 	= mysqli_query($conn, "SELECT * FROM tb_document_category INNER JOIN tb_document_type ON tb_document_category.doc_type_id = tb_document_type.doc_type_id WHERE doc_category_id = '$id' ");
	    	while ($row = mysqli_fetch_array($sql))
	    	    $data[] = $row;
	    	return $data;
	    }

	    function SelectTypeEdit() {
	    	require("../../config.php");
	    	$id 	= $_SESSION['doc_type_id'];
	    	$sql 	= mysqli_query($conn, "SELECT * FROM tb_document_type INNER JOIN tb_employe ON tb_document_type.doc_type_created_by = tb_employe.id_user WHERE doc_type_id = '$id' ");
	    	while ($row = mysqli_fetch_array($sql))
	    	    $data[] = $row;
	    	return $data;
	    }


	}