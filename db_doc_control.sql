-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2019 at 10:58 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_doc_control`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_departemen`
--

CREATE TABLE `tb_departemen` (
  `departemen_id` int(11) NOT NULL,
  `departemen_code` varchar(5) NOT NULL,
  `departemen_name` varchar(100) NOT NULL,
  `departemen_created_at` datetime NOT NULL,
  `departemen_update_at` datetime NOT NULL,
  `departemen_created_by` int(11) NOT NULL,
  `departemen_update_by` int(11) NOT NULL,
  `departemen_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_departemen`
--

INSERT INTO `tb_departemen` (`departemen_id`, `departemen_code`, `departemen_name`, `departemen_created_at`, `departemen_update_at`, `departemen_created_by`, `departemen_update_by`, `departemen_status`) VALUES
(1, '01', 'QC', '2019-11-03 00:00:00', '0000-00-00 00:00:00', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_distributions`
--

CREATE TABLE `tb_distributions` (
  `distribution_id` int(11) NOT NULL,
  `doc_release_id` int(11) NOT NULL,
  `distribution_created_at` datetime NOT NULL,
  `distribution_created_by` int(11) NOT NULL,
  `distribution_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_document`
--

CREATE TABLE `tb_document` (
  `doc_id` int(11) NOT NULL,
  `doc_type_id` int(11) NOT NULL,
  `doc_code` varchar(5) NOT NULL,
  `doc_name` varchar(100) NOT NULL,
  `doc_created_at` datetime NOT NULL,
  `doc_update_at` datetime NOT NULL,
  `doc_created_by` int(11) NOT NULL,
  `doc_update_by` int(11) NOT NULL,
  `doc_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_document_category`
--

CREATE TABLE `tb_document_category` (
  `doc_category_id` int(11) NOT NULL,
  `doc_category_code` varchar(5) NOT NULL,
  `doc_category_name` varchar(100) NOT NULL,
  `doc_category_created_at` datetime NOT NULL,
  `doc_category_update_at` datetime NOT NULL,
  `doc_category_crreated_by` int(11) NOT NULL,
  `doc_category_update_by` int(11) NOT NULL,
  `doc_category_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_document_type`
--

CREATE TABLE `tb_document_type` (
  `doc_type_id` int(11) NOT NULL,
  `doc_category_id` int(11) NOT NULL,
  `doc_type_name` varchar(100) NOT NULL,
  `doc_type_created_at` datetime NOT NULL,
  `doc_type_update_at` datetime NOT NULL,
  `doc_type_created_by` int(11) NOT NULL,
  `doc_type_update_by` int(11) NOT NULL,
  `doc_type_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(16) NOT NULL,
  `level` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `departemen_id` int(11) NOT NULL,
  `user_created_at` datetime NOT NULL,
  `user_update_at` datetime NOT NULL,
  `user_created_by` int(11) NOT NULL,
  `user_update_by` int(11) NOT NULL,
  `status_user` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`user_id`, `username`, `password`, `level`, `name`, `address`, `email`, `phone`, `departemen_id`, `user_created_at`, `user_update_at`, `user_created_by`, `user_update_by`, `status_user`) VALUES
(1, 'admin', 'admin', 1, 'admin', 'Tangerang', 'admin@mail.com', '089999999999', 1, '2019-11-03 00:00:00', '0000-00-00 00:00:00', 1, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_departemen`
--
ALTER TABLE `tb_departemen`
  ADD PRIMARY KEY (`departemen_id`);

--
-- Indexes for table `tb_distributions`
--
ALTER TABLE `tb_distributions`
  ADD PRIMARY KEY (`distribution_id`);

--
-- Indexes for table `tb_document`
--
ALTER TABLE `tb_document`
  ADD PRIMARY KEY (`doc_id`);

--
-- Indexes for table `tb_document_category`
--
ALTER TABLE `tb_document_category`
  ADD PRIMARY KEY (`doc_category_id`);

--
-- Indexes for table `tb_document_type`
--
ALTER TABLE `tb_document_type`
  ADD PRIMARY KEY (`doc_type_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_departemen`
--
ALTER TABLE `tb_departemen`
  MODIFY `departemen_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_distributions`
--
ALTER TABLE `tb_distributions`
  MODIFY `distribution_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_document`
--
ALTER TABLE `tb_document`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_document_category`
--
ALTER TABLE `tb_document_category`
  MODIFY `doc_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_document_type`
--
ALTER TABLE `tb_document_type`
  MODIFY `doc_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
