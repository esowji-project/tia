<!-- Bootstrap core JavaScript-->
<script src="../../../doc-control/asset/package/vendor/jquery/jquery.min.js"></script>
<script src="../../../doc-control/asset/package/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="../../../doc-control/asset/package/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<!-- <script src="../../../doc-control/asset/package/vendor/chart.js/Chart.min.js"></script> -->
<script src="../../../doc-control/asset/package/vendor/datatables/jquery.dataTables.js"></script>
<script src="../../../doc-control/asset/package/vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="../../../doc-control/asset/package/js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="../../../doc-control/asset/package/js/demo/datatables-demo.js"></script>
<!-- <script src="../../../doc-control/asset/package/js/demo/chart-area-demo.js"></script> -->