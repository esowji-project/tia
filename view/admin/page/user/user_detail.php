<?php
    include '../../model/admin-system/select.php';
    $db = new DataDisplayAdminSystem();
    $user = $db -> SelectUser();

    foreach ($user as $data) {
?>

    <div class="container-fluid">    
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php?page=Home">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="index.php?page=User">User</a>
            </li>
            <li class="breadcrumb-item active">Detail User</li>
        </ol>

        <div class="card">
            <div class="card-header">Data Detail</div>
            <div class="card-body">
                <form action="../../../model/admin-system/save.php?admin-system=SaveDataUser">
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Full Name</label>
                                <input type="text" class="form-control" value="<?php echo $data['name']; ?>" readonly>
                            </div>
                            <div class="col-md-6">
                                <label>Departemen</label>
                                <input type="text" class="form-control" value="<?php echo $data['departemen_name']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Address</label>
                                <textarea class="form-control" readonly><?php echo $data['address']; ?></textarea>
                            </div>
                            <div class="col-md-6">
                                <label>Email</label>
                                <input type="email" class="form-control" value="<?php echo $data['email']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Mobile</label>
                                <input type="text" class="form-control" value="<?php echo $data['phone']; ?>" readonly>
                            </div>
                            <div class="col-md-6">
                                <label>Level</label>
                                <input type="text" class="form-control" value="<?php if ($data['level'] == "1") { echo 'Admin System'; } else if ($data['level'] == "2") { echo 'Admin DC'; } else if ($data['level'] == "3") { echo 'Staff Departemen'; } else if ($data['level'] == "4") { echo 'Head Departemen'; } else { echo 'MR'; } ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Username</label>
                                <input type="text" class="form-control" value="<?php echo $data['username']; ?>" readonly>
                            </div>
                            <div class="col-md-6">
                                <label>Password</label>
                                <input type="password" class="form-control" value="<?php echo $data['password']; ?>" readonly>
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Created Date</label>
                                <input type="text" class="form-control" value="<?php echo $data['user_created_at']; ?>" readonly>
                            </div>
                            <div class="col-md-6">
                                <label>Created By</label>
                                <input type="text" class="form-control" value="<?php if ($data['user_created_by'] == "0") { echo 'Not updated yet'; } else { echo $data['name']; } ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Update Date</label>
                                <input type="text" class="form-control" value="<?php echo $data['user_update_at']; ?>" readonly>
                            </div>
                            <div class="col-md-6">
                                <label>Update By</label>
                                <input type="text" class="form-control" value="<?php if ($data['user_update_by'] == "0") { echo 'Not updated yet'; } else { echo $data['name']; } ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <a href="../../model/admin-system/edit.php?edit_user=<?php echo $data['user_id']; ?>" class="btn btn-success"> Edit</a>
                    <a class="btn btn-danger" href="#" onclick="window.history.back()">Back</a>
                </form>
            </div>
        </div>
        <br>
    </div>
<?php } ?>