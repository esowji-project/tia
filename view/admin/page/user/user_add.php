<?php
    $date = date("Y-m-d H:i:s");

    include '../../model/admin-system/select.php';
    $db         = new DataDisplayAdminSystem();
    $user       = $db -> CodeUser();
    $departemen = $db -> SelectDepartemen();
?>

<div class="container-fluid">    
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="index.php?page=User">User</a>
        </li>
        <li class="breadcrumb-item active">Create User</li>
    </ol>

    <div class="card">
        <div class="card-header">Form Create</div>
        <div class="card-body">
            <form action="../../../model/admin-system/save.php?admin-system=SaveDataUser" method="post">

                <input type="hidden" name="id_user" value="<?php echo $user ?>">
                <input type="hidden" name="user_created_by" value="<?php echo $_SESSION['user']; ?>">

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Departemen</label>
                            <select class="form-control" name="departemen_id" required>
                                <option value="">-- Select Departemen --</option>
                                <?php foreach ($departemen as $dt) { ?>
                                    <option value="<?php echo $dt['departemen_id']; ?>"><?php echo $dt['departemen_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Full Name" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Address</label>
                            <textarea name="addresss" class="form-control" placeholder="Full Address" required></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Email" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Mobile</label>
                            <input type="text" name="phone" class="form-control" placeholder="Mobile" required>
                        </div>
                        <div class="col-md-6">
                            <label>Level</label>
                            <select id="level" class="form-control" name="level" required>
                                <option value="">-- Select Level --</option>
                                <option value="1">Admin System</option>
                                <option value="2">Admin DC</option>
                                <option value="3">Staff Departemen</option>
                                <option value="4">Head Departemen</option>
                                <option value="5">MR</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" placeholder="Username" required>
                        </div>
                        <div class="col-md-6">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Created Date</label>
                            <input type="text" name="user_created_at" class="form-control" value="<?php echo $date; ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label>Created By</label>
                            <input type="text" class="form-control" value="<?php echo $_SESSION['name']; ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-success">Save</button>
                <a class="btn btn-danger" href="#" onclick="window.history.back()">Back</a>
            </form>
        </div>
    </div>
</div>