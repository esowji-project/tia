<div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
        <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">User</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i> Data User
            <div class="btn-index">
                <a href="#" onclick="window.history.back()" class="btn btn-sm btn-danger"><i class="fas fa-undo"></i></a>
                <a href="?page=User/Add" class="btn btn-sm btn-success"><i class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Full Name</th>
                        <th>Departemen</th>
                        <th>Level</th>
                        <th>Created Date</th>
                        <th>Update Date</th>
                        <th>Status User</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        include '../../model/admin-system/select.php';
                        
                        $db     = new DataDisplayAdminSystem();
                        $user   = $db -> SelectUser();
                        $no     = "1";

                        foreach ($user as $data) {
                    ?>
                    <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $data["name"]; ?></td>
                        <td><?php echo $data["departemen_name"]; ?></td>
                        <td>
                            <?php 
                                if ($data["level"] == "1") { 
                                    echo "Admin System" ; 
                                } else if ($data["level"] == "2") { 
                                    echo "Admin DC" ; 
                                } else if ($data["level"] == "3") { 
                                    echo "Departement Staff" ; 
                                } else if ($data["level"] == "4") { 
                                    echo "Departement Head" ; 
                                } else { 
                                    echo "MR"; 
                                } 
                            ?>
                        </td>
                        <td><?php echo $data["user_created_at"]; ?></td>
                        <td><?php echo $data["user_update_at"]; ?></td>
                        <td><?php if ($data["status_employe"] == "1") { echo "Active" ; } else { echo "Not Active"; } ?></td>
                        <td>
                            <a href="../../model/admin-system/edit.php?edit_user=<?php echo $data['employe_id']; ?>" class="btn btn-sm btn-warning"><span class="fa fa-edit"></span> Edit</a>
                            <a href="../../model/admin-system/detail.php?detail_user=<?php echo $data['employe_id']; ?>" class="btn btn-sm btn-primary"><span class="fa fa-info"></span> Detail</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>

</div>