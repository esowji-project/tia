<?php
    include '../../model/admin-system/select.php';

    $date       = date("Y-m-d H:i:s");
    $db         = new DataDisplayAdminSystem();
    $user       = $db -> SelectUser();
     

    foreach ($user as $data) {
?>

    <div class="container-fluid">    
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php?page=Home">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="index.php?page=User">User</a>
            </li>
            <li class="breadcrumb-item active">Edit User</li>
        </ol>

        <div class="card">
            <div class="card-header">Form Edit</div>
            <div class="card-body">
                <form action="../../../model/admin-system/update.php?admin-system=UpdateDataUser" method="post">

                    <input type="hidden" name="id_user" value="<?php echo $data['id_user']; ?>">
                    <input type="hidden" name="employe_id" value="<?php echo $data['employe_id']; ?>">
                    <input type="hidden" name="user_created_by" value="<?php echo $_SESSION['user']; ?>">

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Full Name</label>
                                <input type="text" name="name" class="form-control" value="<?php echo $data['name']; ?>" required>
                            </div>
                            <div class="col-md-6">
                                <label>Departemen</label>
                                <select class="form-control" name="departemen_id" required>
                                    <option value="<?php echo $data['departemen_id']; ?>"><?php echo $data['departemen_name']; ?></option>
                                    <option value="">-- Select Departemen --</option>
                                    <?php foreach ($departemen as $dt) { ?>
                                        <option value="<?php echo $dt['departemen_id']; ?>"><?php echo $dt['departemen_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Address</label>
                                <textarea name="address" class="form-control" required><?php echo $data['address']; ?></textarea>
                            </div>
                            <div class="col-md-6">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" value="<?php echo $data['email']; ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Mobile</label>
                                <input type="text" name="phone" class="form-control" value="<?php echo $data['phone']; ?>" required>
                            </div>
                            <div class="col-md-6">
                                <label>Level</label>
                                <select class="form-control" name="level" required>
                                    <option value="<?php echo $data['level']; ?>">
                                        <?php 
                                            if ($data['level']=='1') {
                                                echo "Admin System"; 
                                            } elseif ($data['level']=='2') {
                                                echo "Admin DC"; 
                                            } elseif ($data['level']=='3') {
                                                echo "Staff"; 
                                            } elseif ($data['level']=='4') {
                                                echo "Departemen Head"; 
                                            } else {
                                                echo 'MR';
                                            }
                                        ?>
                                    </option>
                                    <option value="">-- Select Level --</option>
                                    <option value="1">Admin System</option>
                                    <option value="2">Admin DC</option>
                                    <option value="3">Staff</option>
                                    <option value="4">Departemen Head</option>
                                    <option value="5">MR</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Username</label>
                                <input type="text" name="username" class="form-control" value="<?php echo $data['username']; ?>" required>
                            </div>
                            <div class="col-md-6">
                                <label>Password</label>
                                <input type="text" name="password" class="form-control" value="<?php echo $data['password']; ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Created Date</label>
                                <input type="text" name="user_created_at" class="form-control" value="<?php echo $data['user_created_at']; ?>" readonly>
                            </div>
                            <div class="col-md-6">
                                <label>Created By</label>
                                <input type="text" name="user_created_by" class="form-control" value="<?php echo $data['name']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Update Date</label>
                                <input type="text" name="user_update_at" class="form-control" value="<?php echo $date; ?>" readonly>
                            </div>
                            <div class="col-md-6">
                                <label>Update By</label>
                                <input type="text" name="user_update_by" class="form-control" value="<?php echo $data['name']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Status Employe</label>
                                <select class="form-control" name="status_employe" required>
                                    <option value="<?php echo $data['status_employe']; ?>">
                                        <?php 
                                            if ($data['status_employe']=='o') {
                                                echo "Not Active"; 
                                            } else {
                                                echo 'Active';
                                            }
                                        ?>
                                    </option>
                                    <option value="">-- Select Status --</option>
                                    <option value="1">Active</option>
                                    <option value="0">Not Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <button class="btn btn-success">Update</button>
                    <a class="btn btn-danger" href="#" onclick="window.history.back()">Back</a>
                </form>
            </div>
        </div>
        <br>
    </div>
<?php } ?>