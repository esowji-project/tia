<?php
    $date = date("Y-m-d H:i:s");
?>

<div class="container-fluid">    
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="index.php?page=Document">Document</a>
        </li>
        <li class="breadcrumb-item active">Create Document</li>
    </ol>

    <div class="card">
        <div class="card-header">Form Create</div>
        <div class="card-body">
            <form action="../../../model/admin-system/save.php?admin-system=SaveDataDocument" method="post">
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <input type="text" id="doc_code" class="form-control" placeholder="Document Code" required="required" autofocus="autofocus">
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="doc_name" class="form-control" placeholder="Document Name" required="required" autofocus="autofocus">
                        </div>
                        
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <select id="doc_category_id" class="form-control" name="doc_category_id" required>
                                <option value="">-- Select Category Document --</option>
                                <option></option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select id="doc_category_id" class="form-control" name="doc_category_id" required>
                                <option value="">-- Select Type Document --</option>
                                <option></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <input type="text" id="created_at" class="form-control" value="<?php echo $date; ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <input type="text" id="created_by" class="form-control" value="<?php echo "Admin DC"; ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <a class="btn btn-success" href="login.html">Save</a>
                <a class="btn btn-danger" href="#" onclick="window.history.back()">Back</a>
            </form>
        </div>
    </div>
</div>