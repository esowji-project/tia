<div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Document</li>
    </ol>

    <!-- Data Document-->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i> Data Document
            <div class="btn-index">
                <a href="#" onclick="window.history.back()" class="btn btn-sm btn-danger"><i class="fas fa-undo"></i></a>
                <a href="?page=Document/Add" class="btn btn-sm btn-success"><i class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Document Code</th>
                        <th>Document Name</th>
                        <th>Category Document</th>
                        <th>Type Document</th>
                        <th>Created Date</th>
                        <th>Created By</th>
                        <th>Update Date</th>
                        <th>Update By</th>
                        <th>Document Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Tiger Nixon</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                    </tr>
                </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>

</div>