<?php
    include '../../model/admin-system/select.php';

    $date       = date("Y-m-d H:i:s");
    $db         = new DataDisplayAdminSystem();
    $user       = $db -> SelectUser();
    $departemen = $db -> SelectDepartemen();

    foreach ($user as $data) {
?>

    <div class="container-fluid">    
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php?page=Home">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="index.php?page=Departemen">Departemen</a>
            </li>
            <li class="breadcrumb-item active">Edit Departemen</li>
        </ol>

        <div class="card">
            <div class="card-header">Form Edit</div>
            <div class="card-body">
                <form action="../../../model/admin-system/update.php?admin-system=UpdateDataDepartemen" method="post">

                    <input type="hidden" name="departemen_id" class="form-control" value="<?php echo $data['departemen_id']; ?>">

                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Departement Code</label>
                                <input type="text" name="departemen_code" class="form-control" value="<?php echo $data['departemen_code']; ?>" required>
                            </div>
                            <div class="col-md-6">
                                <label>Departement Name</label>
                                <input type="text" name="departemen_name" class="form-control" value="<?php echo $data['departemen_name']; ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Created Date</label>
                                <input type="text" name="departemen_created_at" class="form-control" value="<?php echo $data['departemen_created_at']; ?>" readonly>
                            </div>
                            <div class="col-md-6">
                                <label>Created By</label>
                                <input type="text" name="departemen_created_by" class="form-control" value="<?php echo $data['name']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Update Date</label>
                                <input type="text" name="departemen_update_at" class="form-control" value="<?php echo $date; ?>" readonly>
                            </div>
                            <div class="col-md-6">
                                <label>Update By</label>
                                <input type="text" name="departemen_update_by" class="form-control" value="<?php echo $data['name']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label>Status Departemen</label>
                                <select class="form-control" name="departemen_status" required>
                                    <option value="<?php echo $data['departemen_status']; ?>">
                                        <?php 
                                            if ($data['departemen_status']=='o') {
                                                echo "Not Active"; 
                                            } else {
                                                echo 'Active';
                                            }
                                        ?>
                                    </option>
                                    <option value="">-- Select Status --</option>
                                    <option value="1">Active</option>
                                    <option value="0">Not Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <button class="btn btn-success">Update</button>
                    <a class="btn btn-danger" href="#" onclick="window.history.back()">Back</a>
                </form>
            </div>
        </div>
        <br>
    </div>
<?php } ?>