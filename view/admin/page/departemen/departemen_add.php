<?php
    $date = date("Y-m-d H:i:s");
?>
<div class="container-fluid">    
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="index.php?page=Departemen">Departemen</a>
        </li>
        <li class="breadcrumb-item active">Create Departemen</li>
    </ol>

    <div class="card">
        <div class="card-header">Form Create</div>
        <div class="card-body">
            <form action="../../../model/admin-system/save.php?admin-system=SaveDataDepartemen" method="post">

                <input type="hidden" value="departemen_created_by" value="<?php echo $_SESSION['user']; ?>">
                <input type="hidden" value="departemen_update_at" class="form-control" value="">
                <input type="hidden" value="departemen_update_by" class="form-control" value="">
                <input type="hidden" value="departemen_status" class="form-control" value="1">

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Departemen Code</label>
                            <input type="text" name="departemen_code" class="form-control" placeholder="Departemen Code" required="required" autofocus="autofocus">
                        </div>
                        <div class="col-md-6">
                            <label>Departemen Name</label>
                            <input type="text" name="departemen_name" class="form-control" placeholder="Departemen Name" required="required">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Created Date</label>
                            <input type="text" name="departemen_created_at" class="form-control" value="<?php echo $date; ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label>Created By</label>
                            <input type="text" class="form-control" value="<?php echo $_SESSION['name']; ?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <a class="btn btn-success" href="login.html">Save</a>
                <a class="btn btn-danger" href="#" onclick="window.history.back()">Back</a>
            </form>
        </div>
    </div>
</div>