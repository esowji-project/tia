<?php
    $date = date("Y-m-d H:i:s");
    include '../../model/admin-system/select.php';
    $db         = new DataDisplayAdminSystem();
    $type       = $db -> SelectTypeEdit();

    foreach ($type as $data) {
?>
<div class="container-fluid">    
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="index.php?page=Type">Type</a>
        </li>
        <li class="breadcrumb-item active">Edit Type Document</li>
    </ol>

    <div class="card">
        <div class="card-header">Form Edit</div>
        <div class="card-body">
            <form action="../../model/admin-system/update.php?admin-system=UpdateDataDocType" method="post">

                <input type="hidden" name="doc_type_id" value="<?php echo $data['doc_type_created_id']; ?>">
                <input type="hidden" name="doc_type_created_by" value="<?php echo $data['doc_type_created_by']; ?>">
                <input type="hidden" name="doc_type_update_by" value="<?php echo $_SESSION['user']; ?>">

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Type Name</label>
                            <input type="text" name="doc_type_name" class="form-control" value="<?php echo $data['doc_type_name'] ?>" required="required">
                        </div>
                        <div class="col-md-6">
                            <label>Created at</label>
                            <input type="text" name="doc_type_created_at" class="form-control" value="<?php echo $data['doc_type_created_at'] ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>created By</label>
                            <input type="text" class="form-control" value="<?php echo $_SESSION['name']; ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label>Update at</label>
                            <input type="text" name="doc_type_update_at" class="form-control" value="<?php echo $date ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Update By</label>
                            <input type="text" class="form-control" value="<?php echo $data['name'] ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label>Status Type</label>
                            <select class="form-control" name="doc_type_status" required>
                                <option value="<?php echo $data['doc_type_status']; ?>">
                                    <?php 
                                        if ($data['doc_type_status']=='o') {
                                            echo "Not Active"; 
                                        } else {
                                            echo 'Active';
                                        }
                                    ?>
                                </option>
                                <option value="">-- Select Status --</option>
                                <option value="1">Active</option>
                                <option value="0">Not Active</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-success">Update</button>
                <a class="btn btn-danger" href="#" onclick="window.history.back()">Back</a>
            </form>
        </div>
    </div>
</div>
<?php } ?>