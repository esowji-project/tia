<?php
    $date = date("Y-m-d H:i:s");
    include '../../model/admin-system/select.php';
    $db         = new DataDisplayAdminSystem();
    $type       = $db -> CodeType();
?>
<div class="container-fluid">    
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="index.php?page=Type">Type</a>
        </li>
        <li class="breadcrumb-item active">Create Type Document</li>
    </ol>

    <div class="card">
        <div class="card-header">Form Create</div>
        <div class="card-body">
            <form action="../../model/admin-system/save.php?admin-system=SaveDataType" method="post">

                <input type="hidden" name="doc_type_id" value="">
                <input type="hidden" name="doc_type_created_by" value="<?php echo $_SESSION['user']; ?>">
                <input type="hidden" name="doc_type_update_at" value="">
                <input type="hidden" name="doc_type_update_by" value="">
                <input type="hidden" name="doc_type_status" value="1">

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <input type="text" name="doc_type_code" class="form-control" placeholder="Type Code" value="<?php echo $type; ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="doc_type_name" class="form-control" placeholder="Type Name" required="required">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <input type="text" name="doc_type_created_at" class="form-control" value="<?php echo $date; ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" value="<?php echo $_SESSION['name']; ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-success">Save</button>
                <a class="btn btn-danger" href="#" onclick="window.history.back()">Back</a>
            </form>
        </div>
    </div>
</div>