<div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
        <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Type</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i> Data Type Document
            <div class="btn-index">
                <a href="#" onclick="window.history.back()" class="btn btn-sm btn-danger"><i class="fas fa-undo"></i></a>
                <a href="?page=Document/Type/Add" class="btn btn-sm btn-success"><i class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Type Name</th>
                            <th>Created Date</th>
                            <th>Created By</th>
                            <th>Update Date</th>
                            <th>Update By</th>
                            <th>status Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            include '../../model/admin-system/select.php';
                            $db = new DataDisplayAdminSystem();
                            $type = $db -> SelectType();

                            $no = "1";

                            foreach ($type as $data) {
                        ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $data["doc_type_name"]; ?></td>
                            <td><?php echo $data["doc_type_created_at"]; ?></td>
                            <td><?php if ($data['doc_type_created_by'] == "0") { echo 'Not updated yet'; } else { echo $data['name']; } ?></td>
                            <td><?php echo $data["doc_type_update_at"]; ?></td>
                            <td><?php if ($data["doc_type_update_by"] == "0") { echo "Not updated yet" ; } else { echo $data["name"]; } ?></td>
                            <td><?php if ($data["doc_type_status"] == "1") { echo "Active" ; } else { echo "Not Active"; } ?></td>
                            <td>
                                <a href="../../model/admin-system/edit.php?edit_type=<?php echo $data['doc_type_id']; ?>" class="btn btn-sm btn-warning"><span class="fa fa-edit"></span> Edit</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>

</div>