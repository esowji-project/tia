<?php
    $date = date("Y-m-d H:i:s");

    include '../../model/admin-system/select.php';
    $db = new DataDisplayAdminSystem();
    $code = $db -> CodeCategory();
    $type = $db -> SelectType();
?>
<div class="container-fluid">    
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="index.php?page=Category">Category</a>
        </li>
        <li class="breadcrumb-item active">Create Category Document</li>
    </ol>

    <div class="card">
        <div class="card-header">Form Create</div>
        <div class="card-body">
            <form action="../../model/admin-system/save.php?admin-system=SaveDataCategory" method="post">

                <input type="hidden" name="doc_category_id" value="">
                <input type="hidden" name="doc_category_created_by" value="<?php echo $_SESSION['user'] ?>">
                <input type="hidden" name="doc_category_update_at" value="">
                <input type="hidden" name="doc_category_update_by" value="">
                <input type="hidden" name="doc_category_status" value="1">

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Category Code</label>
                            <input type="text" name="doc_category_code"  value="<?php echo $code; ?>" class="form-control" readonly>
                        </div>
                        <div class="col-md-6">
                            <label>Category Name</label>
                            <input type="text" name="doc_category_name" class="form-control" placeholder="Category Name" required="required">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Type</label>
                            <select class="form-control" name="doc_type_id" required>
                                <option value="">-- Select Type --</option>
                                <?php foreach ($type as $dt) { ?>
                                    <option value="<?php echo $dt['doc_type_id']; ?>"><?php echo $dt['doc_type_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Category Cretaed at</label>
                            <input type="text" name="doc_category_created_at" class="form-control" value="<?php echo $date; ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Category Cretaed By</label>
                            <input type="text" class="form-control" value="<?php echo $_SESSION['name']; ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-success">Save</button>
                <a class="btn btn-danger" href="#" onclick="window.history.back()">Back</a>
            </form>
        </div>
    </div>
</div>