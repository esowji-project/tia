<div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
        <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Category</li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i> Data Category Document
            <div class="btn-index">
                <a href="#" onclick="window.history.back()" class="btn btn-sm btn-danger"><i class="fas fa-undo"></i></a>
                <a href="?page=Document/Category/Add" class="btn btn-sm btn-success"><i class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Category Code</th>
                            <th>Category Name</th>
                            <th>Created Date</th>
                            <th>Created By</th>
                            <th>Update Date</th>
                            <th>Update By</th>
                            <th>status Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            include '../../model/admin-system/select.php';
                            $db = new DataDisplayAdminSystem();
                            $category = $db -> SelectCategory();

                            $no = "1";

                            foreach ($category as $data) {
                        ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $data["doc_category_code"]; ?></td>
                            <td><?php echo $data["doc_category_name"]; ?></td>
                            <td><?php echo $data["doc_category_created_at"]; ?></td>
                            <td><?php echo $data["doc_category_created_by"]; ?></td>
                            <td><?php echo $data["doc_category_update_at"]; ?></td>
                            <td><?php if ($data["doc_category_update_by"] == "0") { echo "Null" ; } else { echo $data["name"]; } ?></td>
                            <td><?php if ($data["doc_category_status"] == "1") { echo "Active" ; } else { echo "Not Active"; } ?></td>
                            <td>
                                <a href="../../model/admin-system/edit.php?edit_category=<?php echo $data['doc_category_id']; ?>" class="btn btn-sm btn-warning"><span class="fa fa-edit"></span> Edit</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>

</div>