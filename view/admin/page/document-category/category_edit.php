<?php
    $date = date("Y-m-d H:i:s");

    include '../../model/admin-system/select.php';
    $db         = new DataDisplayAdminSystem();
    $category   = $db -> SelectCategoryEdit();
    $type       = $db -> SelectCategory();

    foreach ($category as $data) {
?>
<div class="container-fluid">    
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.php?page=Home">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="index.php?page=Category">Category</a>
        </li>
        <li class="breadcrumb-item active">Create Category Document</li>
    </ol>

    <div class="card">
        <div class="card-header">Form Create</div>
        <div class="card-body">
            <form action="../../../model/admin-system/save.php?admin-system=SaveDataCategory" method="post">
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <input type="text" name="category_code"  value="<?php echo $code; ?>" class="form-control" placeholder="Category Code" required="required" autofocus="autofocus">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="category_name" class="form-control" placeholder="Category Name" required="required">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <input type="text" name="category_created_at" class="form-control" value="<?php echo $date; ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="category_created_by" class="form-control" value="<?php echo "Admin DC"; ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-success">Save</button>
                <a class="btn btn-danger" href="#" onclick="window.history.back()">Back</a>
            </form>
        </div>
    </div>
</div>

<?php } ?>