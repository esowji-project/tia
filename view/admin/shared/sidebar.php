

<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item active">
    <a class="nav-link" href="index.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
    </a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Master Data</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="?page=User">Data User</a>
            <a class="dropdown-item" href="?page=Departemen">Data Departemen</a>
            <a class="dropdown-item" href="?page=Document">Data Document</a>
            <a class="dropdown-item" href="?page=Document/Category">Data Doc Category</a>
            <a class="dropdown-item" href="?page=Document/Type">Data Doc Type</a>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-file-alt"></i>
            <span>Document Controll</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown1">
            <a class="dropdown-item" href="?page=ppd">PPD</a>
            <a class="dropdown-item" href="?page=perubahan-dokumen">Perubahan Dokumen</a>
            <a class="dropdown-item" href="?page=daftar-departemen">Daftar Departemen</a>
            <a class="dropdown-item" href="?page=pemusnahan-dokumen">Pemusnahan Dokumen</a>
            <a class="dropdown-item" href="?page=dokumen-external">Dokumen External</a>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-check-double"></i>
            <span>Approval</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown2">
            <a class="dropdown-item" href="?page=approval-departemen">Approval Departemen</a>
            <a class="dropdown-item" href="?page=approval-mr">Approval MR</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="?page=distribusi-dokumen">
            <i class="fas fa-fw fa-share-square"></i>
            <span>Distribusi Dokumen</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="?page=laporan">
            <i class="fas fa-fw fa-file-invoice"></i>
            <span>Laporan</span>
        </a>
    </li>
</ul>